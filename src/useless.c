///////////////////////////////////////////////////////////////////
// Useless
// A poem transformation program
//
// David Llewellyn-Jones
// http://www.flypig.co.uk
//
// October 2016
//
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
// Includes

#include <stdio.h>
#include <string.h>
#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>
#include <argp.h>

///////////////////////////////////////////////////////////////////
// Defines

#define MAX_LINE_LEN (255u)

///////////////////////////////////////////////////////////////////
// Structures and enumerations

///////////////////////////////////////////////////////////////////
// Global variables

const char *argp_program_version = "useless 0.01";
const char *argp_program_bug_address = "<david@flypig.co.uk>";
static char doc[] = "A pretty useless poem transformation program.";
static struct argp_option options[] = {
	{"datadir", 'd', "path", 0, "Data directory to use (defaults to '" USELESSDIR "')"},
	{0}
};

typedef struct {
	char * datadir;
} arguments;

static const int colour[12][2] = {
	{0, 31}, // Red
	{1, 31}, // Light red
	{0, 33}, // Brown
	{1, 33}, // Yellow
	{1, 32}, // Light green
	{0, 32}, // Green
	{0, 36}, // Cyan
	{1, 36}, // Light cyan
	{1, 34}, // Light blue
	{0, 34}, // Blue
	{0, 35}, // Purple
	{1, 35}  // Light purple
};

///////////////////////////////////////////////////////////////////
// Function prototypes

char * generate_data_path (char const * leaf, arguments const * args);

///////////////////////////////////////////////////////////////////
// Function definitions

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
	arguments * args = state->input;
	switch (key) {
		case 'd':
			args->datadir = realloc(args->datadir, strlen(arg) + 1);
			strcpy(args->datadir, arg);
			break;
		default:
			return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp argp = {options, parse_opt, NULL, doc, 0, 0, 0 };

char * generate_data_path (char const * leaf, arguments const * args) {
	char * result = NULL;
	int length;

	if (leaf) {
		length = snprintf(NULL, 0, "%s/%s", args->datadir, leaf);
		result = malloc(length + 2);
		snprintf(result, length + 2, "%s/%s", args->datadir, leaf);
	}

	return result;
}

int main (int argc, char **argv) {
	char * filename;
	arguments args;
	PCRE2_UCHAR8 * regexp = (PCRE2_UCHAR8 *)"[Dd]ong";
	PCRE2_UCHAR8 * subs = (PCRE2_UCHAR8 *)"Pobble";
	PCRE2_UCHAR8 line[MAX_LINE_LEN];
	PCRE2_UCHAR8 conv[MAX_LINE_LEN];
	PCRE2_UCHAR8 * read;
	FILE * dong;
	size_t length;
	pcre2_code * code;
	int errnum;
	PCRE2_SIZE offset;
	int stanza;

	// Parse the command line arguments
	args.datadir = malloc(strlen(USELESSDIR) + 1);
	strcpy(args.datadir, USELESSDIR);
	argp_parse(& argp, argc, argv, 0, 0, & args);
	printf("datadir: %s\n", args.datadir);

	// Open the file using the datadir prefix
	filename = generate_data_path ("dong.txt", & args);
	printf("Opening poem file: %s\n\n", filename);

	// Open the file containing poetry
	dong = fopen(filename, "r");
	if (dong != NULL) {
		// Compile the regular expression to substitution to apply to the poem
		code = pcre2_compile(regexp, PCRE2_ZERO_TERMINATED, 0, & errnum, & offset, NULL);
		if (code) {
			stanza = 0;
			while (!feof(dong)) {
				// Read in a line from the poem
				read = (PCRE2_UCHAR8 *)fgets((char *)line, MAX_LINE_LEN, dong);
				line[MAX_LINE_LEN - 1] = 0;
				if (read) {
					// Apply the substitution to the line
					length = MAX_LINE_LEN;
					pcre2_substitute(code, read, strlen((char *)read), 0, PCRE2_SUBSTITUTE_GLOBAL, NULL, NULL, subs, strlen((char *)subs), conv, &length);
					// Print the result to stdout
					printf ("\e[%d;%dm%s", colour[(stanza % 12)][0], colour[(stanza % 12)][1], conv);
					if (length <= 1) {
						stanza++;
					}
				}
			}
		}
		else {
			// Error compiling the regexp
		  pcre2_get_error_message(errnum, conv, MAX_LINE_LEN);
			printf("Regex compile error, %s (pos %lu)\n", conv, offset);
		}
	}
	else {
		// Error opening the poem file
		printf("Couldn't open file: %s\n", filename);
	}

	printf ("\e[0m");

	free(filename);
	free(args.datadir);

	// All paths lead here
	return 0;
}

